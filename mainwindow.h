#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>    // used for open file dialogue
#include <QStandardPaths> // used to get the default path to documents folder on the system
#include <QImageReader>   // used to verify, if the selected image by user is readable
#include <QMessageBox>    // used to display an error message to the user, if an invalid image was selected in the open file dialog
#include "editor.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /**
     * Slot for handling the "Open" button click
     */
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;

signals:
    /**
     * Used to pass the path to the selected valid file to the Editor window
     * @param filePath path to the image file selected by user
     */
    void exposeFileName(QString filePath);
};

#endif // MAINWINDOW_H
