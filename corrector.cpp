#include "corrector.h"

// Default constructor which initializes the popularPoints to 0's and resets the Hough matrix
Corrector::Corrector()
{
    for (int i = 0; i < popularPointsSize; i++)
    {
        popularPoints[i] = HoughPoint(0,0,0);
    }
    for (int i = 0; i < houghSizeX; i++)
        for (int j = 0; j < houghSizeY; j++)
            Hough[i][j] = 0;
}

// Returns a QRect with coordinates which surround the dark text/lines/etc. in the document, excludong all margins
QRect Corrector::NoMargins(const QImage * image)
{
    // Detect margins
    // the top margin can be anywhere on the document
    int top = GetTop(image, QRect(0, 0, image->width(), image->height()));

    // the bottom margin cannot be aboce the top margin
    int bottom = GetBottom(image, QRect(0, top, image->width(), image->height() - top));

    // the left margin cannot be detected above the top or below the bottom margins,
    // because there is nothing there
    int left = GetLeft(image, QRect(0, top, image->width(), bottom - top));

    // the right margin cannot be detected above the top or below the bottom margins,
    // nor can it be more leftwards than the left margin
    int right = GetRight(image, QRect(left, top, image->width() - left, bottom - top));

    QRect cropped = QRect(left, top, right-left, bottom-top);
    return cropped;
}

// Looks for the top margin
/*
 * The algorith starts searching through each line, starting from the top and
 * looks for a pixel which is black and the pixel above it is white.
 *
 * If such a pixel is not found, the top of searchArea is returned
 */
int Corrector::GetTop(const QImage * image, QRect searchArea)
{
    for (int y = searchArea.top(); y <= searchArea.bottom(); y++)
    {
        int end;
        int start;

        // We check where the black pixels from the right start
        for (end = searchArea.right(); end >= searchArea.left(); end--)
        {
            if (!isBlack(image, end, y))
                break;
        }

        // We check where the black pixels from the left end
        for (start = searchArea.left(); start <= searchArea.right(); start++)
        {
            if (!isBlack(image, start, y))
                break;
        }

        for (int x = start + 1; x < end; x++)
        {
            if (isBlack(image, x, y) && !isBlack(image, x, y - 1))
            {
                return y;
            }
        }
    }
    return searchArea.top();
}

// Looks for the bottom margin
/*
 * The algorith starts searching through each line, starting from the bottom and
 * looks for a pixel which is black and the pixel below it is white.
 *
 * If such a pixel is not found, the bottom of searchArea is returned
 */
int Corrector::GetBottom(const QImage * image, QRect searchArea)
{
    for (int y = searchArea.bottom(); y >= searchArea.top(); y--)
    {
        int end;
        int start;

        // We check where the black pixels from the right start
        for (end = searchArea.right(); end >= searchArea.left(); end--)
        {
            if (!isBlack(image, end, y))
                break;
        }

        // We check where the black pixels from the left end
        for (start = searchArea.left(); start <= searchArea.right(); start++)
        {
            if (!isBlack(image, start, y))
                break;
        }

        for (int x = start + 1; x < end; x++)
        {
            if (isBlack(image, x, y) && !isBlack(image, x, y + 1))
            {
                return y;
            }
        }
    }
    return searchArea.bottom();
}

// Looks for the left margin
/*
 * The algorith starts searching through each column, starting from the left and
 * looks for a pixel which is black and the pixel on the left of it is white.
 *
 * If such a pixel is not found, the left border of searchArea is returned
 */
int Corrector::GetLeft(const QImage * image, QRect searchArea)
{
    for (int x = searchArea.left(); x <= searchArea.right(); x++)
    {
        int end;
        int start;

        // We check where the black pixels from the bottom end
        for (end = searchArea.bottom() - 1; end >= searchArea.top(); end--)
        {
            if (!isBlack(image, x, end))
                break;
        }

        // We check where the black pixels from the top end
        for (start = searchArea.top(); start <= searchArea.bottom(); start++)
        {
            if (!isBlack(image, x, start))
                break;
        }

        for (int y = start + 1; y < end; y++)
        {
            if (isBlack(image, x, y) && !isBlack(image, x - 1, y))
            {
                return x;
            }
        }
    }
    return searchArea.left();
}

// Looks for the right margin
/*
 * The algorith starts searching through each column, starting from the right and
 * looks for a pixel which is black and the pixel on the right of it is white.
 *
 * If such a pixel is not found, the right border of searchArea is returned.
 */
int Corrector::GetRight(const QImage * image, QRect searchArea)
{
    for (int x = searchArea.right(); x >= searchArea.left(); x--)
    {
        int end;
        int start;

        // We check where the black pixels from the bottom end
        for (end = searchArea.bottom() - 1; end >= searchArea.top(); end--)
        {
            if (!isBlack(image, x, end))
                break;
        }

        // We check where the black pixels from the top end
        for (start = searchArea.top(); start <= searchArea.bottom(); start++)
        {
            if (!isBlack(image, x, start))
                break;
        }

        for (int y = start + 1; y < end; y++)
        {
            if (isBlack(image, x, y) && !isBlack(image, x + 1, y))
            {
                return x;
            }
        }
    }
    return searchArea.right();
}

// Looks for the angle, at which the document is rotated
/*
 * It does so iteratively:
 *  First the CalculateAngle function is called with a big step and large startAngle and endAngle.
 *  After that the CalculateAngle function is called again, but with a small step and smaller angles for searching.
 *  At the last step, the CalculateAngle function is called again, but this time with spep and angles, used for fine-tuning.
 */
float Corrector::GetAngleOfRotation(const QImage * image)
{
    float angle = CalculateAngle(image, initialStep, -initialAngleRange/2, initialAngleRange/2);
    qDebug() << angle;

    angle = CalculateAngle(image, secondaryStep, angle - secondaryAngleRange/2, angle + secondaryAngleRange/2);
    qDebug() << angle;

    angle = CalculateAngle(image, lastStep, angle - lastAngleRange/2, angle + lastAngleRange/2);
    qDebug() << angle;

    return -angle;
}

// Calculates the angle of rotation, given an image, a step, a startAngle and an endAngle
/*
 * The algorithm uses Hough transform to detect the angle.
 * @param image: the image, that will be looked at;
 * @param step: a single unit of rotation (the difference between the angles that are checked),
 *              i.e. the first angle, that is checked is startAngle, the next one is startAngle + step, etc;
 * @param startAngle: the angle, at which the first check has to be performed;
 * @param endAngle: the angle, at which the last check has to be performed;
 * @returns float: the angle, at which image is rotated.
 */
float Corrector::CalculateAngle(const QImage * image, float step, float startAngle, float endAngle)
{
    // Goes through each pixel of the image
    for (int y = 0; y < image->height() - 1; y++)
    {
        for (int x = 0; x < image->width() - 1; x++)
        {
            // if there is a black pixel, mark all lines that pass through it as possible candidates for popular points
            if (isBlack(image, x, y) && !isBlack(image, x, y+1))
            {
                // loops through all of the possible angles between startAngle and endAngle with a minimal difference = step
                for (float alpha = startAngle; alpha < endAngle; alpha += step)
                {
                    int d = (int)(y * qCos(qDegreesToRadians(alpha)) - x * qSin(qDegreesToRadians(alpha)));
                    int index = (int)((alpha - startAngle) / step);
                    Hough[index][d] += 1;
                }
            }
        }
    }

    // calculate the diagonal of the image, there is no need to check for distances from the center of the image,
    // which are greater than the diagonal of the image.
    int diagonal = qSqrt( image->width() * image->width() + image->height() * image->height() );

    // Adds each possible line, which is marked in the Hough matrix as a potential popular Hough point
    // and resets the hough point back to zero
    for (int distance = 0; distance < diagonal && distance < houghSizeY; distance++)
    {
        for (int x = 0; x < (endAngle - startAngle) / step; x++)
        {
            if (Hough[x][distance])
            {
                float angle = x * step + startAngle;
                HoughPoint newPoint = HoughPoint(angle, distance, Hough[x][distance]);

                AddToPopularPoints(newPoint);
                Hough[x][distance] = 0;
            }
        }
    }

    // The angle, which will be outputed
    float angle = 0;

    // The angle is equal to the average of all popular points
    for (int i = 0; i < popularPointsSize; i++)
        angle += popularPoints[i].angle;
    angle /= popularPointsSize;

    // Reset the popular points
    ResetPopularPoints();

    return angle;
}

// Checks if and where a candidate for a popular point has to be added
void Corrector::AddToPopularPoints(HoughPoint point)
{
    for (int i = 0; i < popularPointsSize; i++)
    {
        if (point.count > popularPoints[i].count)
        {
            InsertIntoPopularPointsAt(point, i);
            break;
        }
    }
}

// Adds a new HoughPoint at a given position and pushes the points after it with one position.
void Corrector::InsertIntoPopularPointsAt(HoughPoint point, int index)
{
    for (int i = popularPointsSize - 2; i > index; i--)
    {
        popularPoints[i+1] = popularPoints[i];
    }
    popularPoints[index] = point;
}

// A helper fnction which simply prints all of the most pupular points.
void Corrector::PrintPopularPoints()
{
    qDebug() << "====Start====";
    for (int i = 0; i < popularPointsSize; i++)
    {
        qDebug() << popularPoints[i].angle << "\t" << popularPoints[i].distance << "\t" << popularPoints[i].count;
    }
    qDebug() << "====End====";
}

// Resets the popular points to 0
void Corrector::ResetPopularPoints()
{
    for (int i = 0; i < popularPointsSize; i++)
    {
        popularPoints[i].angle = 0;
        popularPoints[i].count = 0;
    }
}

// Checks if a pixel is classified as black or not.
bool Corrector::isBlack(const QImage * image, int x, int y)
{
    return image->pixelColor(x, y).black() > blackPixelTreshold;
}
