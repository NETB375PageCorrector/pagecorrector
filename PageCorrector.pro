#-------------------------------------------------
#
# Project created by QtCreator 2016-10-12T14:37:27
#
#-------------------------------------------------

QT       += core gui widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PageCorrector
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    editor.cpp \
    state.cpp \
    history.cpp \
    corrector.cpp \
    houghpoint.cpp \
    preview.cpp

HEADERS  += mainwindow.h \
    editor.h \
    state.h \
    history.h \
    corrector.h \
    houghpoint.h \
    preview.h

FORMS    += mainwindow.ui \
    editor.ui

RESOURCES = \
    oxygen.qrc

QMAKE_CXXFLAGS+="-gdwarf-2"
а
