#ifndef EDITOR_H
#define EDITOR_H

#include <QWidget>
#include <QImageReader>     // used to read the selected image file by the user in previous window into pixelMap (image)
#include <QImage>           // used to store and save the pixelMap (image)
#include <QColor>           // used for pixel retreival from the image
#include <QStatusBar>       // used for status bar
#include <QLabel>           // used for status bar fileName and status labels
#include <QGraphicsView>    // used to preview the image
#include <QMatrix>          // used for pixelMap (QImage) rotation
#include <QString>          // used to store the path to save pixelMap (QImage) to
#include <QFileDialog>      // used to retreive the path to save pixelMap (QImage) to
#include <QStandardPaths>   // used to retreive the default path to the documents folder in the OS

#include <preview.h>      // Helper class for image preview
#include <state.h>          // Helper class for Statess
#include <history.h>        // Helper class for History

namespace Ui {
class Editor;
}

class Editor : public QWidget
{
    Q_OBJECT

public:
    explicit Editor(QWidget *parent = 0);
    ~Editor();

private:
    Ui::Editor *ui;
    Preview * imgWidget = new Preview();

    /**
     * Rotate a pixelMap
     * @param currentState Current State
     * @param newState     New State to save changes to
     * @param deg          Angle of rotation in degress
     */
    void rotatePixelMap(State *&currentState, State *&newState, float deg);

    /**
     * original State is used for "Revert to original"
     * lastRotation State is used as a buffer for rotations
     * current State is used to hold current State
     */
    State * original, * lastRotation, *current;
    History Undo, Redo; // Undo and Redo histories

    /**
     * Render the new state to preview
     * @param state new State
     */
    void displayNewPixelMapState(State * state);

public slots:
    /**
     * Public slot to receive the path to file to read from
     * @param filePath path to file
     */
    void receiveFileName(QString filePath);

private slots:
    void on_horizontalSlider_valueChanged(int value); // update the LCDDisplay to let the user preview the selected angle of rotation
    void on_horizontalSlider_sliderReleased();        // rotate the pixelMap on slider release. The action was separated into different slot in order to maintain reasonable perfomance
    void on_pushButton_saveTo_clicked();              // "Save" button clicked
    void on_pushButton_revertToOriginal_clicked();    // "Revert to Original" button clicked
    void on_toolButton_undo_clicked();                // "Undo" button clicked
    void on_toolButton_redo_clicked();                // "Redo" button clicked
    void on_toolButton_RotateCW_clicked();            // "Rotate Clockwise" button clicked
    void on_toolButton_RotateCCW_clicked();           // "Rotate Counter-Clockwise" button clicked
    void on_toolButton_RotateFlip_clicked();          // "Flip" button clicked
    void on_Editor_unlockEditingAndSaving();          // Slot for unlock editing and saving signal
    void on_Editor_updateHistoryButtons();            // Slot for updateHistoryButtons signal

signals:
    /**
     * Unlock the UI controls used for Editing and Saving the image, which are disabled by default. Call when the image has been read and the automatic processing has finished.
     */
    void unlockEditingAndSaving();
    /**
     * Verify if the histories are not empty individually, and if they are, disable the corresponding buttons
     */
    void updateHistoryButtons();
};


#endif // EDITOR_H
