#include "preview.h"

#include <QDebug>
#include <QtMath>

// Constructor of the class, create the scene and set default parameters
Preview::Preview(bool isContextualMenu, QWidget * parent) :
    QGraphicsView(parent)
{
    // Set default zoom factors
    zoomFactor=DEFAULT_ZOOM_FACTOR;
    zoomCtrlFactor=DEFAULT_ZOOM_CTRL_FACTOR;

    // Create the scene
    scene = new QGraphicsScene();

    // Allow mouse tracking even if no button is pressed
    this->setMouseTracking(true);

    // Add the scene to the QGraphicsView
    this->setScene(scene);

    // Update all the view port when needed, otherwise, the drawInViewPort may experience trouble
    this->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    // When zooming, the view stay centered over the mouse
    this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    this->setResizeAnchor(QGraphicsView::AnchorViewCenter);

    // Initialize contextual menu if requested
    if (isContextualMenu)
    {
        setContextMenuPolicy(Qt::CustomContextMenu);
        connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&)));
    }

    // Disable scroll bar to avoid a unwanted resize recursion
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // Add the default pixmap at startup
    pixmapItem = scene->addPixmap(pixmap);

    // Initialize the rubberBand
    rubberBand = new QRubberBand(QRubberBand::Rectangle, this);
}


// Destructor of the class
Preview::~Preview()
{
    delete pixmapItem;
    delete scene;

    delete rubberBand;
    delete rubberBandSceneRect;
}



// Display contextual menu
void Preview::showContextMenu(const QPoint & pos)
{
    // Get the mouse position in the scene
    QPoint globalPos = mapToGlobal(pos);
    // Create the menu and add action
    QMenu contextMenu;
    contextMenu.addAction("Reset view", this, SLOT(fitImage()));
    // Display the menu
    contextMenu.exec(globalPos);
}



// Set or update the image in the scene
void Preview::setImage(const QImage & image)
{
    // Update the pixmap in the scene
    pixmap=QPixmap::fromImage(image);
    pixmapItem->setPixmap(pixmap);

    // Resize the scene (needed is the new image is smaller)
    scene->setSceneRect(QRect (QPoint(0,0),image.size()));

    // Store the image size
    imageSize = image.size();

    // As a default, set the rubberBand to occupy the whole image
    rubberBand->setGeometry(image.rect());
}


// Set an image from raw data
void Preview::setImageFromRawData(const uchar * data, int width, int height, bool mirrorHorizontally, bool mirrorVertically)
{
    // Convert data into QImage
    QImage image(data, width, height, width*3, QImage::Format_RGB888);

    // Update the pixmap in the scene
    pixmap=QPixmap::fromImage(image.mirrored(mirrorHorizontally,mirrorVertically));
    pixmapItem->setPixmap(pixmap);

    // Resize the scene (needed is the new image is smaller)
    scene->setSceneRect(QRect (QPoint(0,0),image.size()));

    // Store the image size
    imageSize = image.size();

    // As a default, set the rubberBand to occupy the whole image
    rubberBand->setGeometry(image.rect());
}

// Returns the QRubberBand rect, as defined in the scene
QRect* Preview::getRubberBandSceneRect()
{
    return rubberBandSceneRect;
}

// change the area that the rubberBand occupies
void Preview::setRubberBandRect(const QRect &rect)
{
    QRect newRect;

    QPointF topLeft = mapFromScene(rect.topLeft());
    QPointF bottomRight = mapFromScene(rect.bottomRight());

    newRect.setTopLeft(topLeft.toPoint());
    newRect.setBottomRight(bottomRight.toPoint());

    rubberBand->setGeometry(newRect);

    rubberBandSceneRect = new QRect(rect);
}

// In case the viewport has been changed, we have to remap the rubberBand's geometry from the scene to the viewport
void Preview::refreshRubberBandInViewPort()
{
    QPolygon polygon = mapFromScene(rubberBandSceneRect->x(), rubberBandSceneRect->y(),
                 rubberBandSceneRect->width(), rubberBandSceneRect->height());

    // We get the top left and bottom right points from the polygon and create a QRect, using them
    QRect viewPortRect(polygon.at(0), polygon.at(2));

    rubberBand->setGeometry(viewPortRect);
}

// Displays the rubberBand
void Preview::showRubberBand()
{
    rubberBand->show();
}

// Hides the rubberBand
void Preview::hideRubberBand()
{
    rubberBand->hide();
}



// Fit the image in the widget
void Preview::fitImage()
{
    // Get current scroll bar policy
    Qt::ScrollBarPolicy	currentHorizontalPolicy = horizontalScrollBarPolicy();
    Qt::ScrollBarPolicy	currentverticalPolicy = verticalScrollBarPolicy();

    // Disable scroll bar to avoid a margin around the image
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    // Fit the scene in the QGraphicsView
    this->fitInView(scene->sceneRect(), Qt::KeepAspectRatio);

    // Restaure scroll bar policy
    setHorizontalScrollBarPolicy(currentHorizontalPolicy);
    setVerticalScrollBarPolicy(currentverticalPolicy);
}

// A function that changes the cursor's shape to a given shape
void Preview::changeCursorTo(Qt::CursorShape shape)
{
    QCursor newCursor = cursor();
    newCursor.setShape(shape);
    setCursor(newCursor);
}

// Called when a mouse button is pressed
void Preview::mousePressEvent(QMouseEvent *event)
{
    // Drag mode : change the cursor's shape
    if(event->button() == Qt::MiddleButton)
    {
        origin = QPoint(event->x(), event->y());
        viewPortCenterOrigin = mapToScene(viewport()->rect().center());
        changeCursorTo(Qt::ClosedHandCursor);
    }
    // rubberBand manipulation mode
    else if(event->button() == Qt::LeftButton)
    {
        // if the user pesses the left mouse button
        // while the cursor of the mouse is close to
        // the left edge, we should notify that
        // the left edge should follow the cursor
        if(rubberBandLeftShouldMoveOnClick)
            rubberBandLeftIsMoving = true;
        // if the user pesses the left mouse button
        // while the cursor of the mouse is close to
        // the right edge, we should notify that
        // the right edge should follow the cursor
        else if(rubberBandRightShouldMoveOnClick)
            rubberBandRightIsMoving = true;

        // if the user pesses the left mouse button
        // while the cursor of the mouse is close to
        // the top edge, we should notify that
        // the top edge should follow the cursor
        if(rubberBandTopShouldMoveOnClick)
            rubberBandTopIsMoving = true;
        // if the user pesses the left mouse button
        // while the cursor of the mouse is close to
        // the bottom edge, we should notify that
        // the bottom edge should follow the cursor
        else if(rubberBandBottomShouldMoveOnClick)
            rubberBandBottomIsMoving = true;
    }

    QGraphicsView::mousePressEvent(event);
}

// Called when a mouse button is pressed
void Preview::mouseReleaseEvent(QMouseEvent *event)
{
    // Exit drag mode : change the cursor's shape
    if(event->button() == Qt::MiddleButton)
    {
        changeCursorTo(Qt::ArrowCursor);
    }
    // Exit rubberBand manipulation mode : we reset all the borderIsMoving checks
    else if(event->button() == Qt::LeftButton)
    {
        rubberBandLeftIsMoving = false;
        rubberBandRightIsMoving = false;
        rubberBandTopIsMoving = false;
        rubberBandBottomIsMoving = false;
    }

    QGraphicsView::mouseReleaseEvent(event);
}


#ifndef QT_NO_WHEELEVENT

// Call when there is a scroll event (zoom in or zoom out)
void Preview::wheelEvent(QWheelEvent *event)
{
    if(!(event->buttons() & Qt::MiddleButton))
    {
        // When zooming, the view stay centered over the mouse
        this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

        double factor = (event->modifiers() & Qt::ControlModifier) ? zoomCtrlFactor : zoomFactor;
        if(event->delta() > 0)
            // Zoom in
            scale(factor, factor);
        else
            // Zooming out
            scale(1.0 / factor, 1.0 / factor);

        // The event is processed
        event->accept();
    }
    refreshRubberBandInViewPort();
}

#endif


// Overload the mouse MoveEvent to display the tool tip
void Preview::mouseMoveEvent(QMouseEvent *event)
{
    // Get the coordinates of the mouse in the scene
    QPointF imagePoint = mapToScene(QPoint(event->x(), event->y()));
    // Call the function that create the tool tip
    setToolTip(setToolTipText(QPoint((int)imagePoint.x(),(int)imagePoint.y())));

    // Reset all the bounryShouldMoveOnClick checks when we move the mouse
    rubberBandLeftShouldMoveOnClick = false;
    rubberBandRightShouldMoveOnClick = false;
    rubberBandTopShouldMoveOnClick = false;
    rubberBandBottomShouldMoveOnClick = false;

    // If the user is pressing the middle button and is moving the mouse,
    // we have to remap the center of the viewport according to the current position of the mouse
    if (event->buttons() & Qt::MiddleButton)
    {
        // the center of the viewport, when the middle mouse button was pressed minus
        // how much the mouse has been moved since then divided by the zoom factor
        newCenter = QPointF(viewPortCenterOrigin.x() - (event->x() - origin.x()) / matrix().m11(),
                            viewPortCenterOrigin.y() - (event->y() - origin.y()) / matrix().m11());
        centerOn(newCenter);
    }
    // If the user is not panning, we check if the mouse is close to the edges and if they should be moved
    else
    {
        // If the mouse cursor is within the 5 pixels from the rubberBand,
        // we have to check if it is on some of the edge(s)
        if (rubberBandSceneRect->marginsAdded(QMargins(5, 5, 5, 5)).contains(imagePoint.x(), imagePoint.y()))
        {
            // If the mouse cursor is within 5 pixels from the left edge,
            // the user can click to move it so we have to change the cursor shape to indicate for it
            if (qFabs(rubberBandSceneRect->left() - imagePoint.x()) < 5)
            {
                changeCursorTo(Qt::SizeHorCursor);
                rubberBandLeftShouldMoveOnClick = true;
            }
            // If the mouse cursor is within 5 pixels from the right edge,
            // the user can click to move it and we indicate that
            else if(qFabs(rubberBandSceneRect->right() - imagePoint.x()) < 5)
            {
                changeCursorTo(Qt::SizeHorCursor);
                rubberBandRightShouldMoveOnClick = true;
            }

            // If the mouse cursor is within 5 pixels from the top edge,
            // the user can click to move it and we indicate that
            if (qFabs(rubberBandSceneRect->top() - imagePoint.y()) < 5)
            {
                rubberBandTopShouldMoveOnClick = true;
                // If the user is close to the left edge as well,
                // we have to indicate that its possible to move both
                // by changing the shape of the cursor appropriately
                if (rubberBandLeftShouldMoveOnClick)
                    changeCursorTo(Qt::SizeFDiagCursor);
                // If the user is close to the right edge as well,
                // we have to indicate that its possible to move both
                // by changing the shape of the cursor appropriately
                else if (rubberBandRightShouldMoveOnClick)
                    changeCursorTo(Qt::SizeBDiagCursor);
                // Otherwise, we change the cursor's shape to indicate that
                // the user can move only the top edge
                else
                    changeCursorTo(Qt::SizeVerCursor);
            }
            // If the mouse cursor is within 5 pixels from the bottom edge,
            // the user can click to move it and we indicate that
            else if(qFabs(rubberBandSceneRect->bottom() - imagePoint.y()) < 5)
            {
                rubberBandBottomShouldMoveOnClick = true;
                // If the user is close to the right edge as well,
                // we have to indicate that its possible to move both
                // by changing the shape of the cursor appropriately
                if (rubberBandRightShouldMoveOnClick)
                    changeCursorTo(Qt::SizeFDiagCursor);
                // If the user is close to the left edge as well,
                // we have to indicate that its possible to move both
                // by changing the shape of the cursor appropriately
                else if (rubberBandLeftShouldMoveOnClick)
                    changeCursorTo(Qt::SizeBDiagCursor);
                // Otherwise, we change the cursor's shape to indicate that
                // the user can move only the bottom edge
                else
                    changeCursorTo(Qt::SizeVerCursor);
            }
        }

        // If the mouse cursor is not close to any of the edges,
        // we have to indicate that the user cannot move any of them
        // by changing the cursor's shape to the default one
        if (!rubberBandLeftShouldMoveOnClick &&
            !rubberBandRightShouldMoveOnClick &&
            !rubberBandTopShouldMoveOnClick &&
            !rubberBandBottomShouldMoveOnClick)
        {
            changeCursorTo(Qt::ArrowCursor);
        }



        // If the user is trying to move an edge / edges of the rubberBand

        // if the user wants to move the left edge, we make the left edge follow the mouse's position on X
        if (rubberBandLeftIsMoving)
        {
            rubberBandSceneRect->setLeft(imagePoint.x());
        }
        // if the user wants to move the right edge, we make the right edge follow the mouse's position on X
        else if(rubberBandRightIsMoving)
        {
            rubberBandSceneRect->setRight(imagePoint.x());
        }

        // if the user wants to move the top edge, we make the top edge follow the mouse's position on Y
        if (rubberBandTopIsMoving)
        {
            rubberBandSceneRect->setTop(imagePoint.y());
        }
        // if the user wants to move the bottom edge, we make the bottom edge follow the mouse's position on Y
        else if(rubberBandBottomIsMoving)
        {
            rubberBandSceneRect->setBottom(imagePoint.y());
        }
    }

    refreshRubberBandInViewPort();

    // Call the parent's function (for dragging)
    QGraphicsView::mouseMoveEvent(event);
}


// Overload the function to draw over the image
void Preview::drawForeground(QPainter *painter, const QRectF&)
{

    // Call the function to draw over the image
    this->drawOnImage(painter,imageSize);

    // Reset transformation and call the function draw in the view port
    painter->resetTransform();

    // Call the function to draw in the view port
    this->drawInViewPort(painter, this->viewport()->size());
}


// Overloaded functionthat catch the resize event
void Preview::resizeEvent(QResizeEvent *event)
{
    // First call, the scene is created
    if(event->oldSize().width() == -1 || event->oldSize().height() == -1) return;

    // Get the previous rectangle of the scene in the viewport
    QPointF P1=mapToScene(QPoint(0,0));
    QPointF P2=mapToScene(QPoint(event->oldSize().width(),event->oldSize().height()));

    // Stretch the rectangle around the scene
    if (P1.x()<0) P1.setX(0);
    if (P1.y()<0) P1.setY(0);
    if (P2.x()>scene->width()) P2.setX(scene->width());
    if (P2.y()>scene->height()) P2.setY(scene->height());

    // Fit the previous area in the scene
    this->fitInView(QRect(P1.toPoint(),P2.toPoint()),Qt::KeepAspectRatio);

    refreshRubberBandInViewPort();
}





// Define the virtual function to avoid the "unused parameter" warning
QString Preview::setToolTipText(QPoint imageCoordinates)
{
    (void)imageCoordinates;
    return QString("");
}


// Define the virtual function to avoid the "unused parameter" warning
void Preview::drawOnImage(QPainter* , QSize )
{}


// Define the virtual function to avoid the "unused parameter" warning
void Preview::drawInViewPort(QPainter* , QSize )
{}

