/**\brief
 * A class which acts as a collection of 2 public functions which are responsible for
 * calculating the angle of rotation and the margins of the image which is passed as a parameter.
 *
 * The way we have to use the functions is by instantiating an object of the class with the empty
 * constructor and then use its function for detecting the angle of rotation of a scan of a document
 * and/or the function for calculating the margins.
 * In the case where we want to calculate the angle of rotation and the margins, it is advised to
 * get the margins of a well-oriented image, one that has undergone through GetAngleOfRotation and
 * has the angle fixed.
 *
 * \example
 *      QImage myImage = ... // set the image
 *
 *      Corrector corr;
 *      float angle = corr.GetAngleOfRotation(myImage);
 *      rotatePixelMap(<myImage>, angle);
 *
 *      QRect croppedRect = corr.NoMargins(myImage);
 *      myImage = <myImage>.copy(croppedRect);
 *
 * \uses HoughPoint
 *      We store an array of the 20 most popular points that we find which we have to store information about
 *      the distance from the pivot point, the angle that is voted for and the number of votes for that line.
 *
 *
 */

#include <QtMath>
#include <QImage>
#include <QColor>
#include <QDebug>
#include "houghpoint.h"

#ifndef CORRECTOR_H
#define CORRECTOR_H

class Corrector
{
public:

    // Default constructor which initializes the popularPoints to 0's and resets the Hough matrix
    Corrector();

    // Looks for the margins in a given image and returns a QRect without the margins
    /*
     * @param image: the image that will be scanned for margins
     * @returns QRect: a QRect with coordinates which surround the dark text/lines/etc. in the document, excluding all margins
     */
    QRect NoMargins(const QImage * image);

    // Looks for the angle, at which the document is rotated
    /*
     * It does so iteratively:
     *  First the CalculateAngle function is called with a big step and large startAngle and endAngle.
     *  After that the CalculateAngle function is called again, but with a small step and smaller angles for searching.
     *  At the last step, the CalculateAngle function is called again, but this time with step and angles, used for fine-tuning.
     *
     * @param image: the image that will be scanned for its rotation
     * @returns float: the rotation, at which the document is
     */
    float GetAngleOfRotation(const QImage * image);
private:
    // the maximum number of popular points that will be stored
    static const int popularPointsSize = 20;

    // the number of individual angles that will be checked in each step
    static const int houghSizeX = 40;

    // the maximum distance from the center of the image to any pixel of the document
    static const int houghSizeY = 10000;

    // after what value from 0 to 255 will a pixel be considered black
    static const int blackPixelTreshold = 200;

    // the initial step for checking the angle through CalculateAngle
    static constexpr float initialStep = 10;

    // the initial angle range for checking the angle through CalculateAngle
    static const int initialAngleRange = 100;

    // the step for checking the angle through CalculateAngle on the second step
    static constexpr float secondaryStep = 2.5f;

    // the angle range for checking the angle through CalculateAngle on the second step
    static const int secondaryAngleRange = 40;

    // the step for checking the angle through CalculateAngle on the last step
    static constexpr float lastStep = 0.1f;

    // the angle range for checking the angle through CalculateAngle on the last step
    static const int lastAngleRange = 4;

    // the most popular hough points, which will have effect on the angle that is returned in GetAngleOfRotation
    HoughPoint popularPoints[popularPointsSize];

    // The matrix that stores all possible hough points
    int Hough[houghSizeX][houghSizeY];

    // Calculates the angle of rotation, given an image, a step, a startAngle and an endAngle
    /*
     * The algorithm uses Hough transform to detect the angle.
     * @param image: the image, that will be looked at;
     * @param step: a single unit of rotation (the difference between the angles that are checked),
     *              i.e. the first angle, that is checked is startAngle, the next one is startAngle + step, etc;
     * @param startAngle: the angle, at which the first check has to be performed;
     * @param endAngle: the angle, at which the last check has to be performed;
     * @returns float: the angle, at which image is rotated.
     */
    float CalculateAngle(const QImage * image, float step, float startAngle, float endAngle);

    // Looks for the top margin
    /*
     * The algorithm starts searching through each line, starting from the top and
     * looks for a pixel which is black and the pixel above it is white.
     *
     * If such a pixel is not found, the top of searchArea is returned
     *
     * @param image: the image, whose top margin will be detected
     * @param searchArea: the area, within which the margin could be detected
     * @returns int: the top margin
     */
    int GetTop(const QImage * image, QRect searchArea);

    // Looks for the bottom margin
    /*
     * The algorithm starts searching through each line, starting from the bottom and
     * looks for a pixel which is black and the pixel below it is white.
     *
     * If such a pixel is not found, the bottom of searchArea is returned
     *
     * @param image: the image, whose bottom margin will be detected
     * @param searchArea: the area, within which the margin could be detected
     * @returns int: the bottom margin
     */
    int GetBottom(const QImage * image, QRect searchArea);

    // Looks for the left margin
    /*
     * The algorithm starts searching through each column, starting from the left and
     * looks for a pixel which is black and the pixel on the left of it is white.
     *
     * If such a pixel is not found, the left border of searchArea is returned
     *
     * @param image: the image, whose left margin will be detected
     * @param searchArea: the area, within which the margin could be detected
     * @returns int: the left margin
     */
    int GetLeft(const QImage * image, QRect searchArea);

    // Looks for the right margin
    /*
     * The algorithm starts searching through each column, starting from the right and
     * looks for a pixel which is black and the pixel on the right of it is white.
     *
     * If such a pixel is not found, the right border of searchArea is returned.
     *
     * @param image: the image, whose right margin will be detected
     * @param searchArea: the area, within which the margin could be detected
     * @returns int: the right margin
     */
    int GetRight(const QImage * image, QRect searchArea);

    // Checks if and where a candidate for a popular point has to be added
    /*
     * @param point: the point, which is a potential popular point
     */
    void AddToPopularPoints(HoughPoint point);

    // Adds a new HoughPoint at a given position and pushes the points after it with one position.
    /*
     * @param point: the point, that has to be inserted
     * @param index: the index, at which it has to be inserted
     */
    void InsertIntoPopularPointsAt(HoughPoint point, int index);

    // A helper fnction which simply prints all of the most pupular points.
    void PrintPopularPoints();

    // Resets the popular points to 0
    void ResetPopularPoints();

    // Checks if a pixel is classified as black or not.
    bool isBlack(const QImage * image, int x, int y);
};

#endif // CORRECTOR_H
