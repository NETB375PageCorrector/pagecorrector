#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>     // used for debugging, remove on production
#include <QLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(this->width(),this->height()); // make the window not resizable by the user
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // Fetch the path to the image file
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("Open Scanned Image"),
                QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation),
                tr("Images (*.jpg *.jpeg *.bmp *.png *.gif *.tiff)")
            );

    // verify if the user has selected a file, not didn't for example, just close the open file dialogue
    if(!filename.isEmpty())
    {
        // Declaration and Initialization of QImageReader, used to verify if the selected image is valid
        QImageReader reader(filename);
        // Verify if the selected image is valid, Note the "NOT" logic!
        if(!reader.canRead())
        {
            // Define an error message to the user with detailed explanation
            // %1 is a capture group for "filename" argument
            QMessageBox invalidFileSelected(
                                    QMessageBox::Critical,
                                    tr("File cannot be read"),
                                    tr(
                                        "File \" %1 \" cannot be read." "\n"
                                        "Possibe causes:" "\n"
                                        "\n"
                                        "1) It is not a valid image (might be corrupted)" "\n"
                                        "2) An unsupported image was passed in with different file format" "\n"
                                        "3) There are no sufficient rights to read the file"
                                    ).arg(filename),
                                    QMessageBox::Close,
                                    this
                         );
            // Display the error message
            invalidFileSelected.exec();
        } else {
            // Declare and initialize the new window (Editor)
            auto childWindow = new Editor();
            // Connect the signal from this window to the slot of new window (Editor)
            connect(
                        this,
                        SIGNAL(exposeFileName(QString)),
                        childWindow,
                        SLOT(receiveFileName(QString))

            );
            // Pass-in the file path to the selected image file by the user to the new window (Editor)
            emit exposeFileName(filename);
            // Show the new window (Editor)
            childWindow->show();
            // Close the current window
            this->close();
        }
    }
}
