/**\brief A simple container class, used to hold objects with fields angle, distance and count,
 * which are self-explanatory.
 * It has 2 constructors:
 *      a default one, which does nothing and
 *      a constructor with 3 parameters, which set the 3 fields: angle, distance and count.
 */

#ifndef HOUGHPOINT_H
#define HOUGHPOINT_H

class HoughPoint
{
public:
    // A parameter, which holds the angle, at which the line is rotated from the horizontal axis
    float angle;

    // A parameter, which holds the distance from the center to the line
    int distance;

    // A parameter, which holds how many pixels have voted for this point of the Hough matrih / for this line
    int count;

    // The default contructor
    HoughPoint();

    // A constructor for initializing all parameters
    HoughPoint(float angle, int distance, int count);
};

#endif // HOUGHPOINT_H
