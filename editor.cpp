#include "ui_editor.h"
#include "editor.h"
#include "corrector.h"                                  // Helper class

/**
 * Remove before release section
 */
#include <QDebug>                                       // used for debugging, remove before release
#include <QThread>                                      // not used, remove before release
#include <QTime>                                        // used for debugging, remove before release

/**
 * Class constructor
 */
Editor::Editor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Editor)
{
    ui->setupUi(this);
    /**
     * Disable eiditing and saving UI controls
     */
    ui->horizontalSlider->setDisabled(true);            // Disable the horizontalSlider UI control (Editing)
    ui->pushButton_saveTo->setDisabled(true);           // Disable the "Save" Button UI control (Saving)
    ui->pushButton_revertToOriginal->setDisabled(true); // Disable the "Revert to Original" Button UI control (Editing)
    ui->toolButton_RotateCW->setDisabled(true);         // Disable the "Rotate Clockwise" Button UI control (Editing)
    ui->toolButton_RotateCCW->setDisabled(true);        // Disable the "Rotate Counter-Clockwise" Button UI control (Editing)
    ui->toolButton_RotateFlip->setDisabled(true);       // Disable the "Flip" Button UI control (Editing)
    ui->toolButton_undo->setDisabled(true);             // Disable the "Undo" Button UI control (Editing)
    ui->toolButton_redo->setDisabled(true);             // Disable the "Redo" Button UI control (Editing)

    auto bar = new QStatusBar;                          // Declare and initialize the status bar for the windows

    // Insert labels file name and  status labels into status bar
    bar->addPermanentWidget(ui->fileNameLabel);
    bar->addWidget(ui->statusLabel);

    // Insert the status bar into current window
    ui->Parent->addWidget(bar);

    // Connect local (private) signals and slots
    connect(
                this,
                SIGNAL(unlockEditingAndSaving()),
                this,
                SLOT(on_Editor_unlockEditingAndSaving())

    );
    connect(
                this,
                SIGNAL(updateHistoryButtons()),
                this,
                SLOT(on_Editor_updateHistoryButtons())

    );
}

/**
 * Public slot to receive the path to file to read from
 * @param filePath path to file
 */
void Editor::receiveFileName(QString filePath) {
    ui->fileNameLabel->setText(filePath);               // update the file path label in status bar
    ui->statusLabel->setText(tr("Opening..."));         // update the status label in status bar

    QImageReader reader(filePath);                      // Declair and initialize the QImage reader with the received file path
    // Double-check if the image is valid
    if (reader.canRead()) {
        original = new State(0, reader.read());         // Create an original immutable State with values of 0 degrees rotation and original image
        ui->statusLabel->setText(tr("File can be read"));
                                                        // update the status label in status bar
        imgWidget->setImage(original->pixelMap);
                                                        // set the image preview to the original image
        imgWidget->setBackgroundBrush(QBrush(QColor(82, 86, 89)));
                                                        // set out-of-bounds background of the preview
        ui->row1->insertWidget(1, imgWidget);           // insert preview in the middle row of the window
        ui->statusLabel->setText(tr("Initial Preview"));
                                                        // update the status label in status bar
        emit this->unlockEditingAndSaving();            // Unlock editing and saving UI controls
        ui->statusLabel->setText(tr("Post-algorithm Preview"));
                                                        // update the status label in status bar
        current = lastRotation = original;              // modify after production to lastRotation




        Corrector corr;                                 // Helper class to retrieve the correct values for the image correction
        int start = QTime::currentTime().msecsSinceStartOfDay();
                                                        // for perfomance calculation
        float angle = corr.GetAngleOfRotation(&original->pixelMap);
                                                        // retreive the correct angle of rotation of the image
        qDebug() << "=============\nAngle:\t" << QTime::currentTime().msecsSinceStartOfDay() - start << "\n=============";
                                                        // show the perfomance info
        ui->lcdNumber->display(angle);                  // set the value of LCDDisplay to calculated
        ui->horizontalSlider->setValue(angle);          // set the value of horizonalSlider to calculated

        State * buffer1 = new State;
        rotatePixelMap(current, buffer1, angle);
        current = buffer1;
        displayNewPixelMapState(current);               // render new pixelMap State

        start = QTime::currentTime().msecsSinceStartOfDay();
                                                        // for perfomance calculation
        QRect croppedRect = corr.NoMargins(&current->pixelMap);
                                                        // retreive the trimmed image
        qDebug() << "=============\nMargins:\t" << QTime::currentTime().msecsSinceStartOfDay() - start << "\n=============";
                                                        // show the perfomance info

        imgWidget->setRubberBandRect(croppedRect);
        imgWidget->showRubberBand();
    }
}

/**
 * Unlock editing and saving UI elements
 */
void Editor::on_Editor_unlockEditingAndSaving()
{
    ui->horizontalSlider->setDisabled(false);           // Enable the horizontalSlider UI control (Editing)
    ui->pushButton_revertToOriginal->setDisabled(false);
                                                        // Enable the "Revert to Original" Button UI control (Editing)
    ui->toolButton_RotateCW->setDisabled(false);        // Enable the "Rotate Clockwise" Button UI control (Editing)
    ui->toolButton_RotateCCW->setDisabled(false);       // Enable the "Rotate Counter-Clockwise" Button UI control (Editing)
    ui->toolButton_RotateFlip->setDisabled(false);      // Enable the "Flip" Button UI control (Editing)
    ui->pushButton_saveTo->setDisabled(false);          // Enable the "Save" Button UI control (Saving)
}

/**
 * Verify if the histories are not empty individually,
 * and if they are, disable the corresponding buttons
 */
void Editor::on_Editor_updateHistoryButtons()
{
    // Check if Undo History is empty, if is, disable the Undo button, if is not, enable the Undo button
    if (Undo.isEmpty()) ui->toolButton_undo->setDisabled(true);
    else ui->toolButton_undo->setDisabled(false);

    // Check if Redo History is empty, if is, disable the Redo button, if is not, enable the Redo button
    if (Redo.isEmpty()) ui->toolButton_redo->setDisabled(true);
    else ui->toolButton_redo->setDisabled(false);
}

/**
 * Class Destructor
 */
Editor::~Editor()
{
    delete ui;
}

/**
 * Rotate a pixelMap
 * @param currentState Current State
 * @param newState     New State to save changes to
 * @param deg          Angle of rotation in degress
 */
void Editor::rotatePixelMap(State *&currentState, State *&newState, float deg) {
    QMatrix matrix;                                     // Define a new matrix
    matrix.rotate(deg);                                 // Rotate the matrix on user-defined angle
    // Keep the logic consistent to avoid the "over-rotation", resulting in quality-loss or even Image Preview out-of-boundary bugs
    if (deg > 45 || deg < -45) newState = new State(currentState->deg, currentState->pixelMap.transformed(matrix, Qt::SmoothTransformation));                         // apply rotation
    else newState = new State(deg, currentState->pixelMap.transformed(matrix, Qt::SmoothTransformation));
                                                        // apply rotation
}

/**
 * Render the new state to preview
 * @param state new State
 */
void Editor::displayNewPixelMapState(State * state) {
    imgWidget->setImage(state->pixelMap);               // update the preview
    imgWidget->setBackgroundBrush(QBrush(QColor(82, 86, 89)));
                                                        // set out-of-bounds background of the preview
    ui->horizontalSlider->setValue(state->deg*10);      // update the value of horizonalSlider to match the State's angle of rotation value
}

// update the LCDDisplay to let the user preview the selected angle of rotation
void Editor::on_horizontalSlider_valueChanged(int value)
{
    float deg = value;                                  // Retrieve the user-defined desired angle of rotation
    deg /= 10;                                          // divide the angle of rotation by 10
    ui->lcdNumber->display(deg);                        // Update the LCDDisplay value to match the user-defined desired angle of rotation for user's convenience
}

// rotate the pixelMap on slider release. The action was separated into different slot in order to maintain reasonable perfomance
void Editor::on_horizontalSlider_sliderReleased()
{
    float deg = ui->horizontalSlider->value();          // Retrieve the user-defined desired angle of rotation
    deg /= 10;                                          // divide the angle of rotation by 10

    Undo.push(current);                                 // Add the current State to the Undo history
    Redo.clear();                                       // Clear the Redo history
    emit this->updateHistoryButtons();                  // update history buttons
    rotatePixelMap(lastRotation, current, deg);         // Rotate the pixelMap from lastRotation State to Current State on user-defined degrees
    displayNewPixelMapState(current);                   // render new pixelMap State
}

// "Undo" button clicked
void Editor::on_toolButton_undo_clicked()
{
    if (!Undo.isEmpty())
    {
        Redo.push(current);                             // Add the current State to the Redo history
        current = Undo.pop();                           // Retreive the latest State from Undo History
        displayNewPixelMapState(current);               // render new pixelMap State
        qDebug() << "Undo history has operations left:\t" << Undo.length();
                                                        // debugging info, remove on production
    } else {
        qDebug("Undo history is empty!");               // debugging info, remove on production
    }

    emit this->updateHistoryButtons();                  // update history buttons
}

// "Redo" button clicked
void Editor::on_toolButton_redo_clicked()
{
    if (!Redo.isEmpty())
    {
        Undo.push(current);                             // Add the current State to the Undo history
        current = Redo.pop();                           // Retreive the latest State from Redo History
        displayNewPixelMapState(current);               // render new pixelMap State
        qDebug() << "Redo history has operations left:\t" << Undo.length();
                                                        // debugging info, remove on production
    } else {
        qDebug("Redo history is empty!");               // debugging info, remove on production
    }

    emit this->updateHistoryButtons();                  // update history buttons
}

// "Revert to Original" button clicked
void Editor::on_pushButton_revertToOriginal_clicked()
{
    Undo.push(current);                                 // Add the current State to the Undo history
    Redo.clear();                                       // Clear the Redo history
    emit this->updateHistoryButtons();                  // update history buttons
    current = lastRotation = original;                  // Current and lastRotation States are equal to Original State
    displayNewPixelMapState(current);                   // render new pixelMap State
    ui->horizontalSlider->setValue(0);                  // Reset the value in horizonalSlider
}

// "Rotate Clockwise" button clicked
void Editor::on_toolButton_RotateCW_clicked()
{
    Undo.push(current);                                 // Add the current State to the Undo history
    Redo.clear();                                       // Clear the Redo history
    emit this->updateHistoryButtons();                  // update history buttons
    State * buffer1 = new State;                        // Declaring a buffer State
    rotatePixelMap(current, buffer1, 90);               // Rotate a current pixelMap on 90 Clockwise into a buffer State
    current = buffer1;                                  // Load the Buffer State's address into Current State
    State * buffer2 = new State;                        // Declare a buffer State
    rotatePixelMap(lastRotation, buffer2, 90);          // Rotate a lastRotation pixelMap on 90 Clockwise into a buffer State
    lastRotation = buffer2;                             // Load the Buffer State's address into lastRotation State
    displayNewPixelMapState(current);                   // render new pixelMap State
}

// "Rotate Counter-Clockwise" button clicked
void Editor::on_toolButton_RotateCCW_clicked()
{
    Undo.push(current);                                 // Add the current State to the Undo history
    Redo.clear();                                       // Clear the Redo history
    emit this->updateHistoryButtons();                  // update history buttons
    State * buffer1 = new State;                        // Declaring a buffer State
    rotatePixelMap(current, buffer1, -90);              // Rotate a current pixelMap on 90 degress Counter-Clockwise into a buffer State
    current = buffer1;                                  // Load the Buffer State's address into Current State
    State * buffer2 = new State;                        // Declare a buffer State
    rotatePixelMap(lastRotation, buffer2, -90);         // Rotate a lastRotation pixelMap on 90 Counter-Clockwise into a buffer State
    lastRotation = buffer2;                             // Load the Buffer State's address into lastRotation State
    displayNewPixelMapState(current);                   // render new pixelMap State
}

// "Flip" button clicked
void Editor::on_toolButton_RotateFlip_clicked()
{
    Undo.push(current);                                 // Add the current State to the Undo history
    Redo.clear();                                       // Clear the Redo history
    emit this->updateHistoryButtons();                  // update history buttons
    State * buffer1 = new State;                        // Declaring a buffer State
    rotatePixelMap(current, buffer1, 180);              // Rotate a current pixelMap on 180 degress into a buffer State
    current = buffer1;                                  // Load the Buffer State's address into Current State
    State * buffer2 = new State;                        // Declare a buffer State
    rotatePixelMap(lastRotation, buffer2, 180);         // Rotate a lastRotation pixelMap on 180 degrees into a buffer State
    lastRotation = buffer2;                             // Load the Buffer State's address into lastRotation State
    displayNewPixelMapState(current);                   // render new pixelMap State
}

// "Save" button clicked
void Editor::on_pushButton_saveTo_clicked()
{
    QImage cropped = current->pixelMap.copy(*(imgWidget->getRubberBandSceneRect()));
    State * newState = new State(current->deg, cropped);
    current = newState;
    displayNewPixelMapState(current);

    imgWidget->hideRubberBand();

    // Retreive a file path to save the pixelMap (QImage) to
    QString saveFilePath = QFileDialog::getSaveFileName(
                                            this,
                                            tr("Save File"),
                                            QStandardPaths::writableLocation(
                                                QStandardPaths::DocumentsLocation
                                            ),
                                            tr("JPEG (*.jpg *.jpeg);;PNG (*.png)")
                                        );
                                                        // Only JPEG and PNG output formats are supported
    if (!saveFilePath.isEmpty()) current->pixelMap.save(saveFilePath);
                                                        // verify is the file path is not empty (for example, if the user didn't just close the save file path dialogue), and if not, save the pixelMap (QImage) to the file path
}
