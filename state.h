#ifndef STATE_H
#define STATE_H

#include <QImage>


class State
{
public:
    State();
    State(float deg, QImage pixelMap);
    const float deg;
    const QImage pixelMap;
    ~State();
};

#endif // STATE_H
