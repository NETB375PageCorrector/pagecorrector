#ifndef PREVIEW_H
#define PREVIEW_H

#include <QObject>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QMenu>
#include <QRubberBand>


// Default zoom factors
#define         DEFAULT_ZOOM_FACTOR             1.15
#define         DEFAULT_ZOOM_CTRL_FACTOR        1.01




/*!
 * \brief The Preview class       This class has been designed to display an image
 *                                  The image can be zoomed or moved with the mouse
 *                                  By overloading this class, it is possible to draw over the image (drawOnImage)
 *                                  or in the view port (drawInViewPort)
 */
class Preview : public QGraphicsView
{
    Q_OBJECT

public:


    /*!
     * \brief Preview             Constructor, create the scene and set parameters
     * \param isContextualMenu      When set to true, the contextual menu is initialize
     *                              If you don't want contextual menu or need to create a
     *                              custom one, set to false.
     */
    Preview(bool isContextualMenu=true, QWidget * parent=0);
    ~Preview();


    /*!
     * \brief setImage          Set the image in the widget
     * \param image             QImage to display
     */
    void                    setImage(const QImage & image);


    /*!
     * \brief setImageFromRawData   Set the image from raw data
     * \param data                  Pointer to the raw data (data format is RGBRGBRGBRGB ...)
     * \param size                  Size of the image to display
     * \note                        The raw data MUST be compliant with the size
     */
    void                    setImageFromRawData(const uchar * data, int width, int height,
                                                bool mirrorHorizontally=false, bool mirrorVertically=false);


    /*!
     * \brief setZoomFactor     Set the zoom factor when the CTRL key is not pressed
     * \param factor            zoom factor (>1)
     */
    void                    setZoomFactor(const double factor) { zoomFactor=factor; }


    /*!
     * \brief setZoomCtrlFactor     Set the zoom factor when the CTRL key is pressed
     * \param factor                zoom factor (>1)
     */
    void                    setZoomCtrlFactor(const double factor) {zoomCtrlFactor=factor; }

    // Returns the QRubberBand rect, as defined in the scene
    QRect* getRubberBandSceneRect();

    // change the area that the rubberBand occupies
    // @param rect: a QRect which is set as the rubberBand's geometry and the rubberBandSceneRect.
    void setRubberBandRect(const QRect &rect);

    // Displays the rubberBand
    void showRubberBand();

    // Hides the rubberBand
    void hideRubberBand();



public slots:

    /*!
     * \brief fitImage              This function fit the image in the view while keeping aspect ratio
     */
    void                    fitImage();



protected:


    /*!
     * \brief drawOnImage           Overload this function to draw over the image
     * \param painter               Painter for drawing
     * \param imageSize             Size of the image
     */
    virtual void            drawOnImage(QPainter* painter, QSize imageSize);


    /*!
     * \brief drawOnImage           Overload this function to draw in the view port
     * \param painter               Painter for drawing
     * \param imageSize             Size of the view port
     */
    virtual void            drawInViewPort(QPainter* painter, QSize portSize);


    /*!
     * \brief drawForeground        Drawing he foreground of the scene
     *                              When overloading this function, you must call it parent before
     *                              exiting the function, otherwise drawOnImage and drawInViewPort
     *                              will not work properly
     * \param painter               Painter for drawing
     * \param rect                  The area of the scene displayed in the view port
     */
    virtual void            drawForeground(QPainter *painter, const QRectF &rect);


    /*!
     * \brief setToolTipText        Display the tool tip over the mouse
     * \param imageCoordinates      Coordinates of the mouse in the image's frame
     * \return                      The function returns the QString that is displayed over the image
     */
    virtual QString         setToolTipText(QPoint imageCoordinates);


    /*!
     * \brief mousePressEvent       Overload this function to process mouse button pressed
     * \param event                 mouse event
     */
    virtual void            mousePressEvent(QMouseEvent *event);


    /*!
     * \brief mouseReleaseEvent     Overload this function to process mouse button released
     * \param event                 mouse event
     */
    virtual void            mouseReleaseEvent(QMouseEvent *event);


    /*!
     * \brief wheelEvent            Overload this function to process mouse wheel event
     * \param event                 mouse wheel event
     */
    virtual void            wheelEvent(QWheelEvent *event);


    /*!
     * \brief mouseMoveEvent        Overload this function to process mouse moves
     * \param event                 mouse move event
     */
    virtual void            mouseMoveEvent(QMouseEvent *event);


    /*!
     * \brief resizeEvent        Overload this function to process the window resizing event
     * \param event              resize event
     */
    virtual void            resizeEvent(QResizeEvent *event);


protected slots:


    /*!
     * \brief showContextMenu       Display the contextual menu (on right click)
     * \param pos                   Position of the mouse in the widget
     */
    virtual void            showContextMenu(const QPoint & pos);


private:

    // Scene where the image is drawn
    QGraphicsScene*         scene;

    // Pixmap item containing the image
    QGraphicsPixmapItem*    pixmapItem;

    // Size of the current image
    QSize                   imageSize;

    // Zoom factor
    double                  zoomFactor;

    // Zoom factor when the CTRL key is pressed
    double                  zoomCtrlFactor;

    // Current pixmap
    QPixmap                 pixmap;


    // The QRubberBand, which is used to display the area without the margins,
    // which is returned from the Corrector::NoMargins function
    QRubberBand *rubberBand;
    // This is the rectangle, which the rubberBand occupies, relative to the scene / the image of the document.
    QRect *rubberBandSceneRect;

    // indicates if the cursor's position is in proximity to the rubberBand's left edge,
    // therefore should be manipulated upon clicking with the left mouse button
    bool rubberBandLeftShouldMoveOnClick = false;
    // indicates if the cursor's position is in proximity to the rubberBand's right edge,
    // therefore should be manipulated upon clicking with the left mouse button
    bool rubberBandRightShouldMoveOnClick = false;
    // indicates if the cursor's position is in proximity to the rubberBand's top edge,
    // therefore should be manipulated upon clicking with the left mouse button
    bool rubberBandTopShouldMoveOnClick = false;
    // indicates if the cursor's position is in proximity to the rubberBand's bottom edge,
    // therefore should be manipulated upon clicking with the left mouse button
    bool rubberBandBottomShouldMoveOnClick = false;

    // indicates that the user wants to manipulate the left edge, therefore it should follow the cursor's position
    bool rubberBandLeftIsMoving = false;
    // indicates that the user wants to manipulate the right edge, therefore it should follow the cursor's position
    bool rubberBandRightIsMoving = false;
    // indicates that the user wants to manipulate the top edge, therefore it should follow the cursor's position
    bool rubberBandTopIsMoving = false;
    // indicates that the user wants to manipulate the bottom edge, therefore it should follow the cursor's position
    bool rubberBandBottomIsMoving = false;

    // A function that changes the cursor's shape to a given shape
    // @param Qt::CursorShape shape: the new shape that will be applied for the cursor
    void changeCursorTo(Qt::CursorShape shape);

    // In case the viewport has been changed, we have to remap the rubberBand's geometry from the scene to the viewport
    void refreshRubberBandInViewPort();

    /// Vareables for panning.
    // The point, at which the cursor is on the viewport, when the middle button is pressed.
    QPointF origin;
    // the position of the center of the image, relative to the viewport upon pressing the middle button.
    QPointF viewPortCenterOrigin;
    // the current position of the center of the image, relative to the viewport.
    QPointF newCenter;
};

#endif // PREVIEW_H
