# Variant 2. Page corrector

Using the programming language C++ and programming framework Qt implement a computer program that opens an image file that contains the bitmap of a scanned printed document page. The document has not been placed correctly on the scanner and the document lines are not horizontal (but they are all parallel).

The program must have the following features:

- [x] it detects automatically (without the aid of the user) the angle of the document line with the x-axis of the image;
- [x] it fixes the angle by rotating automatically the image;
- [x] it detects and removes the top, bottom, left and right white margins of the page.

# Table of Contents

	1. Opening and Saving images
	2. Reading and Storing images
	3. Pixel, retrieving pixel from image
	4. Manipulating the images
	5. History, undo and redo functionality
	6. User Interface

+++

# 1. Opening and Saving images
## 1.1 Opening images
![Screenshot of the File Open Dialog](http://127.0.0.1:8080/images/Open%20File%20Dialogue.png "Screenshot of the File Open Dialog")

i. We include ```QFileDialog``` class in header file

```c++
	#include <QFileDialog>
```

ii. According to Qt 5.7 documentation

```c
	getOpenFileName(
        QWidget *parent = Q_NULLPTR,
        const QString &caption = QString(),
        const QString &dir = QString(),
        const QString &filter = QString(),
        QString *selectedFilter = Q_NULLPTR,
        Options options = Options()
	)
```
function returns a ```QString```

iii. We use it in our code as follows:

```c++
	QString filename = QFileDialog::getOpenFileName(
        this,
        tr("Open Scanned Image"),
        QStandardPaths::writableLocation(
	        QStandardPaths::DocumentsLocation
        ),
        tr("Images (*.jpg *.jpeg *.bmp *.png *.gif *.tiff)")
    );
```
For better user experience we use ```QStandardPaths``` library to set the default path in dialogue to Documents folder in user’s system

iv. Assuming the possibility of user closing the dialogue, therefore not selecting the file we enclose further code under a simple if statement’s body

```c++
	if (!filename.isEmpty())
	{
	    QImageReader reader(filename);
	    if(!reader.canRead())
	    {
	    }
	}
```

v. We populate the statement body further

```c++
	if (!filename.isEmpty())
	{
	    QImageReader reader(filename);
		if(!reader.canRead())
		{
		}
	}
```
above we use the ```QImageReader``` class to check whether the selected file is a **valid** image.

vi. If it is not we display an error message with help of ```QMessageBox``` file

```c++
	if (!filename.isEmpty())
	{
		QImageReader reader(filename);
	    if(!reader.canRead())
	    {
		    QMessageBox invalidFileSelected(
			  QMessageBox::Critical,
			  tr("File cannot be read"),
		      tr("..."),
		      QMessageBox::Close,
		      this
		    );
			invalidFileSelected.exec();
	    }
	}
```
The code snippet above can be shortened by following a non-API pattern. We used an API approach to have better control over message box. Due to several limitations I had to minimize the code snippet above. The ```tr("...")``` is actually:

```c++
	tr(
        "File \" %1 \" cannot be read." "\n"
        "Possibe causes:" "\n"
        "\n"
        "1) It is not a valid image (might be corrupted)" "\n"
        "2) An unsupported image was passed in with different file format" "\n"
        "3) There are no sufficient rights to read the file"
	).arg(filename),
```
```tr()``` function is used for localization of strings (in other words, translation).
```%1``` is a caption group which displays first argument (filename in our case).

![Screenshot of the Error box](http://127.0.0.1:8080/images/Invalid%20File%20Message%20Box.png "Screenshot of the Error box")

vii. In case image is valid we enclose the further code under else statement

```c++
	if (!reader.canRead())
    {
        ...
    } else {
        auto childWindow = new Editor();

        connect(
	        this, SIGNAL(exposeFileName(QString)),
	        childWindow, SLOT(receiveFileName(QString))
        );

		emit exposeFileName(filename);
        childWindow->show();
        this->close();
	}
```
In the code snippet above the following happens:

+ A new window is created

```c++
	auto childWindow = new Editor();
```

+ The path of selected file is passed to it with the help of Qt’s signals and slots mechanism

```c++
  connect(
	  this,
      SIGNAL(exposeFileName(QString)),
      childWindow,
      SLOT(receiveFileName(QString))
  );

  emit exposeFileName(filename);
```

+ The new window is shown

```c++
	childWindow->show();
```

+ The current window is closed

```c++
	this->close();
```

+++

## 1.2 Saving images
The overall procedure of implementation is quite similar to *Opening files*.

i. We use ```QFileDialog``` class as well, but different function from it.

```c++
	QString saveFilePath = QFileDialog::getSaveFileName(
		this,
	    tr("Save File"),
	    QStandardPaths::writableLocation(
		    QStandardPaths::DocumentsLocation
	    ),
	    tr("JPEG (*.jpg *.jpeg);;PNG (*.png)")
	);
```
The reason why our application saves only in *JPEG* or *PNG* formats is because of ```QImage``` class limitations.

It is capable of both **Reading** and **Writing** in these formats

ii. We further use saveFilePath

```c++
    if (!saveFilePath.isEmpty())
    {
    }
```
In case end user has not selected the filename we are just going to skip the further operation

iii. We further use saveFilePath

```c++
	if (!saveFilePath.isEmpty())
	{
	  current.pixelMap.save(saveFilePath);
	}
```
```current``` is a ```State``` (see *Reading and Storing images*). ```pixelMap``` is a ```QImage``` object.

+++

# 2. Reading and Storing images
Our application requires a data structure to store the selected image. We are going to use ```QImage``` class, instance(s) of which, from now on we may refer to as ```pixelMap(s)```.

```c++
    #include <QImage>
```

The reason why we chose ```QImage``` is because it is a part of Qt ecosystem, so it integrates well with other Qt classes.

Another reason is, that it is able to provide our application crucial functionality without additional complexity of implementation:

+ Getting the pixel from image by coordinates

```c++
    pixel(int x, int y) const
```

+ Saving the image into file

```c++
    save(
        const QString &fileName,
        const char *format = Q_NULLPTR,
        int quality = -1
    ) const
```

+ Rotation of the image

```c++
    transformed(
        const QTransform &matrix,
        Qt::TransformationMode mode = Qt::FastTransformation
    ) const
```

Reading Image file into ```pixelMap``` is done with the help of ```QImageReader``` Qt class

```c++
	QImageReader reader(filePath);
    QImage pixelMap = reader.read();
```

+++

# Pixel, retrieving pixel from image
Our application requires a data structure to store the pixel, extracted from the image. We are going to use ```QColor``` class, instance(s) of which, from now on we may refer to as ```pixel(s)```.

```c++
	#include <QColor>
```

The reason why we chose ```QColor``` is because it is a part of Qt ecosystem, so it integrates well with other Qt classes and we do not have additional complexity, for retrieval of pixel from the image.

```c++
	QColor pixel = pixelMap.pixel(x, y)
```

Another reason is, that it is able to provide our application crucial functionality, without additional complexity of implementation:

+ To retrieve the value of **Red** in pixel (0-255):

```c++
	int red() const
```

+ To retrieve the value of **Green** in pixel (0-255):

```c++
	int green() const
```

+ To retrieve the value of **Red** in pixel (0-255):

```c++
	int red() const
```

+ To retrieve the value of **Black** in pixel (0-255):

```c++
	int black() const
```

+ To retrieve the value of **Lightness** in pixel (0-255):

```c++
	int lightness() const
```

+++

# 4. Manipulating the images
## 4.1 Rotation
i. In order to perform ```pixelMap``` rotation we will have to use ```QMatrix``` Qt class

```c++
	#include <QMatrix>
```
ii. We create a new ```QMatrix```

```c++
	QMatrix rotationMatrix;
```
iii. We rotate the matrix itself

```c++
	rotationMatrix.rotate(90);
```
iv. We apply the rotation to ```pixelMap```

```c++
	pixelMap = pixelMap.transformed(
				rotationMatrix,
				Qt::SmoothTransformation
			   );
```

By default ```transformed``` method uses ```Qt::FastTransformation``` mode, which leaves end result without smoothing and therefore “pixelated”

We fix it by passing ```Qt::SmoothTransformation``` as second argument to ```transformed``` method.

+++

# 5. History, undo and redo functionality
## 5.1 Introduction to the concept of States
In order to implement *undo* and *redo* functionality in our application we had to follow the states pattern, which can be found in [Immutable.js by Facebook](https://facebook.github.io/immutable-js/).
## 5.2 State
```State``` is a custom class, which holds:

+ Angle of image rotation value
+ ```QImage``` pixelMap

In order to keep the values **immutable**, we have declared them as constants:

```c++
	class State
    {
	    public:
		    ...
		    const float deg;
		    const QImage pixelMap;
		    ...

	}
```
They are initialized in the **constructor** of the class:

```c++
	State::State(float deg, QImage pixelMap) :
    deg(deg),
    pixelMap(pixelMap)
    {}
```

We will further use the ```State```(s) for *undo* and *redo* history in our application.
+++
## 5.3 History
In order to implement the *undo* and *redo* functionality in our application we:

i. Define ```History```, which extends ```QStack```

#### History.h

```c++
	#ifndef HISTORY_H
	#define HISTORY_H
	#include <QStack>
	#include "state.h"

	class History: public QStack<State *>
	{
	    public:
		    History();
	};

	#endif // HISTORY_H
```
#### History.cpp

```c++
	#include "history.h"

	History::History()
	{}
```

ii. In the source code for **Editor** window in our application we define three ```State``` and two ```History``` variables as follows:
#### Editor.h
```c++
	#include <state.h>
	#include <history.h>

	class Editor : public QWidget
    {
        ...
        private:
        ...
            State * original, * lastRotation, *current;
            History Undo, Redo;
        ...
    }
```
iii. We further populate our source code for the **Editor** window in our application following the pattern below:

On each major action (for example, image rotation) we push the current state to the ```Undo``` history.
#### Editor.cpp
```c++
    Undo.push(current);
```

The modifications to the current state, as well as "swapping" of the current and the new states are done in functional methods (for example, ```rotatePixelMap```), otherwise manually.
#### Editor.cpp
```c++
	void Editor::rotatePixelMap(
		State *&currentState,
		State *&newState, float deg
	)
	{
	    QMatrix matrix;
	    matrix.rotate(deg);
	    if (deg > 45 || deg < -45)
	    {
		    newState = new State(currentState->deg, currentState->pixelMap.transformed(matrix, Qt::SmoothTransformation));
		}
	    else
	    {
		    newState = new State(deg, currentState->pixelMap.transformed(matrix, Qt::SmoothTransformation));
		}
	}
```

In order to be in control of the performance we are going to handle the rendering to the preview of the new pixelMap manually. We will create a ```displayNewPixelMapState``` method for that purpose.
#### Editor.cpp
```c++
	void Editor::displayNewPixelMapState(State * state)
	{
	    imgWidget->setImage(state->pixelMap);
	    imgWidget->setBackgroundBrush(QBrush(QColor(82, 86, 89)));
	    ui->horizontalSlider->setValue(state->deg*10);
	}
```

As the result, we can manually rotate the image when user slides the slider in UI:
#### Editor.cpp
```c++
	void Editor::on_horizontalSlider_sliderReleased()
	{
	    float deg = ui->horizontalSlider->value();
	    deg /= 10;

	    Undo.push(current);
	    Redo.clear();
	    emit this->updateHistoryButtons();
	    rotatePixelMap(lastRotation, current, deg);
	    displayNewPixelMapState(current);
	}
```

Another two good examples of ```History``` and ```State``` usage pattern could be *undo* and *redo* buttons code snippets

#### Editor.cpp
```c++
	void Editor::on_toolButton_undo_clicked()
	{
	    if (!Undo.isEmpty())
	    {
	        Redo.push(current);
	        current = Undo.pop();
	        displayNewPixelMapState(current);
	        qDebug() << "Undo history has operations left:\t" << Undo.length();
	    }
	    else
	    {
	        qDebug("Undo history is empty!");
	    }

	    emit this->updateHistoryButtons();
	}

```

#### Editor.cpp
```c++
	void Editor::on_toolButton_redo_clicked()
	{
	    if (!Redo.isEmpty())
	    {
	        Undo.push(current);
	        current = Redo.pop();
	        displayNewPixelMapState(current);
	        qDebug() << "Redo history has operations left:\t" << Undo.length();
	    }
	    else
	    {
	        qDebug("Redo history is empty!");
	    }

	    emit this->updateHistoryButtons();
	}

```

In three last cases we have stumbled upon ```updateHistoryButtons``` method. All it does is just verifies if *undo* and *redo* ```History```-s have any actions in stack.

The verification is separate for each ```History```, and in case the ```History``` is **empty**, the corresponding button is **disabled**, otherwise, if it contains **at least one** ```State```, the corresponding button is **enabled**.
#### Editor.cpp
```c++
	void Editor::on_Editor_updateHistoryButtons()
	{
	    if (Undo.isEmpty())
	    {
		    ui->toolButton_undo->setDisabled(true);
		}
	    else
	    {
		    ui->toolButton_undo->setDisabled(false);
		}

	    if (Redo.isEmpty())
	    {
		    ui->toolButton_redo->setDisabled(true);
		}
	    else
	    {
		    ui->toolButton_redo->setDisabled(false);
		}
	}
```
An explanation for ```ui->toolButton_undo->setDisabled(true);``` statement can be found in **6.2 Programmatic access to UI elements** section.


# 6. User Interface
![Screenshot of working application](http://127.0.0.1:8080/images/UI.png "Screenshot of working application")

## 6.1 UI Design
While developing our application we followed along the “Keep it simple, stupid” (KISS) design principle.

We kept the interface as clean as possible, so that end user could pick up the workflow straightaway.

The initial window consists of just a single Button (```QPushButton```) with a title of "Open scanned image".

![Screenshot of the initial window](http://127.0.0.1:8080/images/Open%20File%20Button.png "Screenshot of the initial window")

The editor window has to provide the following functionalities:

+ Preview the image
+ Provide the basic editing and manipulation to the image
+ Provide capability of saving the changed image to the filesystem

In order to allow provide basic editing and manipulation capabilities we:

1. Place an ```QLCDNumber``` to preview the set angle and ```QSlider``` to allow changing the angle;
2. Place "Revert to Original" ```QPushButton``` and conveniently place "Save" ```QPushButton```, wrapped into ```QHBoxLayout``` to the bottom of the window;
3. Place a ```QHBoxLayout``` layout in the middle of the window. The first element of the layout is another layout ```QVBoxLayout```, which contains five ```QPushButton```s. The first two ```QPushButton```s are "Undo" and "Redo", which have History functionality. The last three ```QPushButton```s are "CW" (Clockwise), "CCW" (Counter-Clockwise) and "Flip" (180 degree rotation). In order to save precious workspace for the user, we had to use images inside of those buttons, instead of text. In order to do that, we had to use [Oxygen Icon Set](https://github.com/pasnox/oxygen-icons-png) from [KDE](https://www.kde.org). We had to use the [Qt Resource System](http://doc.qt.io/qt-5/resources.html) to store the icons. Then, we set the ```QPushButton```'s ```icon``` property to ```iconset``` and  ```theme=":/icons/oxygen/edit-redo.png"```;
4. We also need to utilize the status bar for better user experience. The example can be found in the next section.

![Screenshot of the editor window](http://127.0.0.1:8080/images/UI%20Explained.png "Screenshot of the editor window")

## 6.2 Programmatic access to UI elements
In Qt, each element has an API provided to the developer. So, for example, if we need to disable a ```QSlider``` during runtime, we can do it as follows:
#### Editor.cpp
```c++
	ui->horizontalSlider->setDisabled(true);
```

Another great example could be keeping the user informed about status of application by changing the text in the status bar. It can be done the following way:
#### Editor.cpp
```c++
	ui->statusLabel->setText(tr("Post-algorithm Preview"));
```

## 6.3 Signals and Slots mechanism
We have previously stumbled upon
#### Editor.cpp
```c++
	emit this->updateHistoryButtons();
```
It is defined in our ```Editor.h``` under signals
#### Editor.h
```c++
	class Editor : public QWidget
    {
        ...
        signals:

	    void updateHistoryButtons();
    }
```
Slot is defined in our ```Editor.h``` under private slots
#### Editor.h
```c++
	class Editor : public QWidget
    {
        ...
        private slots:

	    void on_Editor_updateHistoryButtons();
    }
```

#### Editor.cpp
```c++
	void Editor::on_Editor_updateHistoryButtons()
	{
	    if (Undo.isEmpty()) ui->toolButton_undo->setDisabled(true);
	    else ui->toolButton_undo->setDisabled(false);

	    if (Redo.isEmpty()) ui->toolButton_redo->setDisabled(true);
	    else ui->toolButton_redo->setDisabled(false);
	}
```

In ```Editor.cpp``` class constructor we connect *Signal* and *Slot*
#### Editor.cpp
```c++
	connect(
	    this,
	    SIGNAL(updateHistoryButtons()),
	    this,
	    SLOT(on_Editor_updateHistoryButtons())
	);
```

As the result when some change occurs, we emit signal

```c++
	emit this->updateHistoryButtons();
```

Which is further received by slot. Further, the slot manipulates UI controls.

We can pass-in values to slots as well

```c++
	emit exposeFileName(filename);
```

However, we need to strictly match the arguments

```c++
	connect(
		this,
		SIGNAL(exposeFileName(QString)),
		childWindow,
		SLOT(receiveFileName(QString))
	);
```
